# This is a simple test file for the example.py script
from example import hello_world

def test_hello_world():
    assert hello_world() == "Hello, world!"
