# This is a simple Python script example
def hello_world():
    return "Hello, world!"

if __name__ == "__main__":
    print(hello_world())
